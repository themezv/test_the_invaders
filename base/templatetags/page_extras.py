from django import template

register = template.Library()


@register.inclusion_tag('base/menu.html', takes_context=True)
def show_menu(context, path):
    request = context['request']
    user = request.user
    context = {
        'path': path,
        'user': user,
    }
    return context


@register.inclusion_tag('base/top.html', takes_context=True)
def show_top(context):
    from base.models import MainSettings
    firm_settings = MainSettings.objects.get(active=True)
    request = context['request']
    user = request.user
    context = {
        'firm_settings': firm_settings,
        'user': user,
    }
    return context
