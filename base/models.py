from django.db import models


class MainSettings(models.Model):
    email = models.EmailField('Email', default='example@gmail.com')
    phone = models.CharField('Номер телефона', max_length=20, default='Номер телефона')
    active = models.BooleanField('Активность параметров', default=False, help_text='Только одни параметры должны быть активны')

    def __str__(self):
        return '{} {}'.format(self.email, self.phone)

    class Meta:
        verbose_name = 'Контактные данные'
        verbose_name_plural = 'Контактные данные'
        unique_together = ('active',)
