from .models import ExtUser, City
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelChoiceField, ModelForm
from django.contrib.auth.models import User


class ExtUserForm(UserCreationForm):
    city = ModelChoiceField(City.objects.all(), to_field_name='name')

    def save(self, commit=True):
        user = super(ExtUserForm, self).save(commit=False)
        if commit:
            user.save()
            extuser = ExtUser()
            extuser.user = user
            extuser.city = City.objects.get(name=self.cleaned_data['city'])
            extuser.save()
        return user


class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class ChooseCity(ModelForm):
    city = ModelChoiceField(City.objects.all(), to_field_name='name')

    class Meta:
        fields = ['city']