from django.db import models
from django.contrib.auth.models import User


class City(models.Model):
    name = models.CharField('Название города', max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class ExtUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name='Город')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Расширенный пользователь'
        verbose_name_plural = 'Расширенные пользователи'
