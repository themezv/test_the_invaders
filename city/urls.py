from django.conf.urls import url
from .views import CreateExtUser, SuccesPage, LogoutPage, ChooseCity, LoginPage
from django.contrib.auth.views import login

urlpatterns = [
    url(r'^auth/$', CreateExtUser.as_view(), name="create_extuser"),
    url(r'^choose_city/$', ChooseCity.as_view(), name="choose_city"),
    url(r'^success/$', SuccesPage.as_view(), name="success"),
    url(r'^logout/$', LogoutPage.as_view(), name="logout"),
    url(r'^login/$', login, {'template_name': 'city/login.html'}, name='login'),
]