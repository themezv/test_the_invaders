from django.views.generic import DetailView, CreateView, FormView, TemplateView, RedirectView
from .forms import ExtUserForm, LoginForm
from django.contrib.auth import login, logout, authenticate
from .models import ExtUser, City
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy


class SuccesPage(TemplateView):
    template_name = 'base/success.html'


class CreateExtUser(FormView):
    template_name = 'city/create_ext_user.html'
    model = ExtUser
    form_class = ExtUserForm
    success_url = reverse_lazy('city:success')

    def form_valid(self, form):
        login(self.request, form.save(commit=True))
        return super(CreateExtUser, self).form_valid(form)


class ChooseCity(CreateView):
    model = ExtUser
    fields = ['city']
    template_name = 'city/choose_city.html'

    def form_valid(self, form):
        print(form.cleaned_data['city'])
        user = User.objects.get(username=form.data['user'])
        extuser, created = ExtUser.objects.get_or_create(user=user)
        extuser.city = City.objects.get(name=form.cleaned_data['city'])
        extuser.save()
        return redirect(reverse_lazy('city:success'))

    def form_invalid(self, form):
        print('invalid')
        print(form)


class LogoutPage(RedirectView):
    pattern_name = 'success'
    url = reverse_lazy('city:success')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutPage, self).get(self, request, *args, **kwargs)


class LoginPage(FormView):
    form_class = LoginForm
    template_name = 'city/login_page.html'
    success_url = reverse_lazy('city:success')

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        if user is not None:
            login(self.request, user)
        else:
            return redirect(reverse_lazy('city:login'))
        return super(LoginPage, self).form_valid(form)

    def form_invalid(self, form):
        return redirect(reverse_lazy('city:login'))
