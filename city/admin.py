from django.contrib import admin
from .models import ExtUser, City
# Register your models here.
admin.site.register(ExtUser)
admin.site.register(City)