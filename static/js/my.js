$(document).ready(
    $('#like, #dislike').on('click', function () {
        $.ajax({
            url: $(this).data('likeUrl'),
            data: {
                action: $(this).attr('id')
            },
            self: this,
            success: function (data) {
                console.log(data['likes_count']);
                if (data['action'] === "like") {
                        $('#likes-count').text(data['likes_count']);
                    }
                else {
                        $('#dislikes-count').text(data['dislikes_count']);
                    }
                $('#like, #dislike').prop('disabled', true);
                if(!data['result']) {
                    alert('Вы уже голосовали')
                }
            },
            error: function () {
                alert('Ошибка');
            }
        })
    }));



