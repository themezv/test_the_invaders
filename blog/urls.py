from django.conf.urls import url
from .views import ArticleListView, ArticleDetailView, ArticleLike, ArticleComment


urlpatterns = [
    url(r'^$', ArticleListView.as_view(), name='article_list'),
    url(r'^(?P<category_slug>[\w-]+)/$', ArticleListView.as_view(), name='article_list'),
    url(r'^article/(?P<slug>[-\w]+)/$', ArticleDetailView.as_view(), name='article_detail'),
    url(r'^article/(?P<slug>[-\w]+)/like/$', ArticleLike, name='article_like'),
    url(r'^article/(?P<slug>[-\w]+)/comment/$', ArticleComment.as_view(), name='article_comment'),
]