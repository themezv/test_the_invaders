from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
from city.models import City, ExtUser
import PIL
from django.core.urlresolvers import reverse_lazy


class ArticleCategory(models.Model):
    title = models.CharField('Название', max_length=150)
    text = RichTextUploadingField(config_name='full')
    slug = models.SlugField('Slug', unique=True, blank=True)

    def get_absolute_url(self):
        return reverse_lazy('article_category', args=[str(self.slug)])

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория статей'
        verbose_name_plural = 'Категории статей'


class Comment(models.Model):
    extuser = models.ForeignKey(ExtUser)
    body = RichTextField(config_name='text')

    def __str__(self):
        return self.extuser.user.username

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'


class Article(models.Model):
    title = models.CharField('Название', max_length=150)
    text = RichTextUploadingField(config_name='full')
    slug = models.SlugField('Slug', unique=True, blank=True)
    city = models.ForeignKey(City, verbose_name='Город')
    date = models.DateTimeField('Время и дата публикации')
    image = models.ImageField('Главное изображение', upload_to='articles/')

    categorys = models.ManyToManyField(ArticleCategory, verbose_name='Категории')

    comments = models.ManyToManyField(Comment, verbose_name='Комментарии', blank=True)
    likes = models.ManyToManyField(ExtUser, related_name='likes', blank=True)
    dislikes = models.ManyToManyField(ExtUser, related_name='dislikes', blank=True)

    def get_likes_count(self):
        return self.likes.count()

    def get_dislikes_count(self):
        return self.dislikes.count()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'