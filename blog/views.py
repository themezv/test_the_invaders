from django.views.generic import ListView, DetailView, FormView
from .models import Article, ArticleCategory, Comment
from .forms import CommentForm
from city.models import ExtUser
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from django.http.response import HttpResponse


class ArticleListView(ListView):
    model = Article
    paginate_by = 3

    # def get(self, request, *args, **kwargs):
    #     if request.user.is_authenticated:
    #         if request.user.extuser:
    #             return super(ArticleListView, self).get(request, *args, **kwargs)
    #         else:
    #             return redirect(reverse_lazy('city:choose_city'))
    #     else:
    #         return redirect(reverse_lazy('city:create_extuser'))

    def get_queryset(self):
        category_slug = self.kwargs.get('category_slug', None)
        if category_slug:
            if hasattr(self.request.user, 'extuser'):
                return Article.objects.filter(city=self.request.user.extuser.city, categorys__slug=category_slug).order_by('date')
            else:
                return Article.objects.filter(categorys__slug=category_slug).order_by('date')
        else:
            if hasattr(self.request.user, 'extuser'):
                return Article.objects.filter(city=self.request.user.extuser.city).order_by('date')
            else:
                return Article.objects.all().order_by('date')

    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)
        if hasattr(self.request.user, 'extuser'):
            context['categories_list'] = ArticleCategory.objects.filter(article__city=self.request.user.extuser.city).distinct()
        else:
            context['categories_list'] = ArticleCategory.objects.all().distinct()
        context['category_slug'] = self.kwargs.get('category_slug', None)
        return context


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'blog/article_detail.html'
    comment_form = CommentForm

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        if hasattr(self.request.user, 'extuser'):
            context['categories_list'] = ArticleCategory.objects.filter(article__city=self.request.user.extuser.city).distinct()
        else:
            context['categories_list'] = ArticleCategory.objects.all().distinct()
        article = Article.objects.get(slug=self.kwargs.get('slug', None))
        if hasattr(self.request.user, 'extuser'):
            if not article.comments.filter(article__comments__extuser=ExtUser.objects.get(user=self.request.user)):
                context['comment_form'] = self.comment_form
        return context


def ArticleLike(request, slug):
    if request.is_ajax():
        extUser = request.user.extuser
        article = Article.objects.get(slug=slug)
        likes = article.likes.all()
        likes_count = len(likes)
        dislikes = article.dislikes.all()
        dislikes_count = len(dislikes)
        answer = {'likes_count': likes_count, 'dislikes_count': dislikes_count}
        if extUser in likes or extUser in dislikes:
            answer.update({'result': False})
            return JsonResponse(answer)
        else:
            action = request.GET.get('action', 'like')
            if action == 'like':
                article.likes.add(extUser)
                likes_count += 1
            else:
                article.dislikes.add(extUser)
                dislikes_count += 1
            answer.update({'result': True, 'action': action, 'likes_count': likes_count, 'dislikes_count': dislikes_count})
            return JsonResponse(answer)
    else:
        return redirect('/')


class ArticleComment(FormView):
    form_class = CommentForm

    def form_valid(self, form):
        slug = self.kwargs.get('slug')
        article = Article.objects.get(slug=slug)
        comments = article.comments.all()
        if not article.comments.filter(article__comments__extuser=ExtUser.objects.get(user=self.request.user)):
            comment = Comment(body=form.cleaned_data['body'], extuser=self.request.user.extuser)
            comment.save()
            article.comments.add(comment)
            return redirect(reverse_lazy('blog:article_detail', args=[slug]))
        else:
            return redirect(reverse_lazy('blog:article_detail', args=[slug]))
